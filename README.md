# LÉEME #

Una vez clonado el repositorio configura tu identidad para que 
sepamos quién realiza los cambios. Para ello, dentro del 
directorio del repositorio, teclea:

     git config user.name "Fulanito de Tal y Cual"
     git config user.email "fulanito@servidor.es"
     
y comprueba que aparece tu correo en el apartado [user] tecleando:

     less .git/config
